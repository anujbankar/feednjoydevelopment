package com.merge;

import java.io.FileInputStream;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class HomePage{

    private LoginController loginController;

    private ScrollPane fxd_scrollpane;

    public HomePage(LoginController loginController) throws Exception{

                this.loginController = loginController;

                initialize();

    }

    public void initialize()throws Exception{

        // Create UI elements
        Label fxd_welcomeLabel = new Label("You Extra Their          \n  Essentials!");
        Label fxd_welcomeLabel1 = new Label("Turning the waste into worth                                                   ");
        Label fxd_welcomeLabel3 = new Label("Give Meal , Give Hope!!!                                                       ");
        fxd_welcomeLabel.setStyle("-fx-font-size: 70px; -fx-font-weight: bold; -fx-text-fill:#FFFFE0;");
        fxd_welcomeLabel1.setStyle("-fx-font-size: 20px;  -fx-text-fill:#FFFFE0;");
        fxd_welcomeLabel3.setStyle("-fx-font-size: 20px;  -fx-text-fill:#FFFFE0;");

        // Style buttons
        String fxd_buttonStyle = "-fx-background-color:#FFFFE0; -fx-background-radius:15;-fx-text-fill: black; -fx-font-size: 14px; -fx-padding: 10px 20px;";
        String fxd_buttonStyle1 = "-fx-background-color:LightBlue; -fx-background-radius:15;-fx-text-fill: black; -fx-font-size: 14px; -fx-padding: 20px 40px;";

        // Create "Join Us" button with styling and tooltip
        Button fxd_btn1 = new Button("Join US");
        fxd_btn1.setStyle(fxd_buttonStyle1);
        fxd_btn1.setTooltip(new Tooltip("Join us in our mission to reduce waste and help others."));
        VBox fxd_vb3Box_btn = new VBox(fxd_btn1);
        fxd_vb3Box_btn.setPrefWidth(100);
        fxd_vb3Box_btn.setPrefHeight(100);
        fxd_vb3Box_btn.setAlignment(Pos.CENTER);

        // Create "Learn More" button with styling and tooltip
        Button fxd_wasteButton = new Button("Learn More");
        fxd_wasteButton.setStyle(fxd_buttonStyle1);
        fxd_wasteButton.setTooltip(new Tooltip("Learn more about how we turn waste into worth."));
        VBox fxd_vbWasteButton = new VBox(fxd_wasteButton);
        fxd_vbWasteButton.setAlignment(Pos.CENTER);

        // Create a VBox layout container with 20px spacing between elements
        VBox fxd_layout = new VBox(20);
        fxd_layout.getChildren().addAll(fxd_welcomeLabel, fxd_welcomeLabel1, fxd_vbWasteButton, fxd_welcomeLabel3, fxd_vb3Box_btn);
        fxd_layout.setStyle("-fx-alignment: center; -fx-padding: 20px;");
        fxd_layout.setPadding(new Insets(200, 100, 0, 0));

        // Create navbar buttons
        Button fxd_homeButton = new Button("Home");
        Button fxd_aboutUsButton = new Button("About Us");
        fxd_aboutUsButton.setOnAction(e->{

            loginController.NavigateToAboutUs();
            
        });
        Button fxd_storiesButton = new Button("Stories");
        fxd_storiesButton.setOnAction(e->{

            loginController.NavigateToStories();
            
        });
        Button fxd_contactUsButton = new Button("Contact Us");
        fxd_contactUsButton.setOnAction(e->{

            loginController.NavigateToContactUs();
            
        });

        // Apply styling to navbar buttons
        fxd_homeButton.setStyle(fxd_buttonStyle);
        fxd_aboutUsButton.setStyle(fxd_buttonStyle);
        fxd_storiesButton.setStyle(fxd_buttonStyle);
        fxd_contactUsButton.setStyle(fxd_buttonStyle);

        // Load logo image
        FileInputStream fxd_fileInputStream1 = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Gauri__Img/hlogo.png");
        Image fxd_logoImage = new Image(fxd_fileInputStream1);
        ImageView fxd_logoImageView = new ImageView(fxd_logoImage);
        fxd_logoImageView.setFitHeight(200);
        fxd_logoImageView.setFitWidth(200);
        fxd_logoImageView.setPreserveRatio(false);

        // Create HBox for navbar buttons with 30px spacing
        HBox fxd_navbar = new HBox(30, fxd_homeButton, fxd_aboutUsButton, fxd_storiesButton, fxd_contactUsButton);
        fxd_navbar.setAlignment(Pos.CENTER_RIGHT);

        // Set horizontal grow priority for navbar buttons
        HBox.setHgrow(fxd_homeButton, Priority.ALWAYS);
        HBox.setHgrow(fxd_aboutUsButton, Priority.ALWAYS);
        HBox.setHgrow(fxd_storiesButton, Priority.ALWAYS);
        HBox.setHgrow(fxd_contactUsButton, Priority.ALWAYS);

        // Create HBox for navbar and move it down with padding
        HBox fxd_navbar1 = new HBox(1200, fxd_navbar);
        fxd_navbar1.setAlignment(Pos.CENTER_RIGHT);
        fxd_navbar1.setPadding(new Insets(20, 0, 0, 0)); // Move navbar down

        // Ensure navbar buttons are aligned to the right
        fxd_homeButton.setMaxWidth(200);
        fxd_aboutUsButton.setMaxWidth(200);
        fxd_storiesButton.setMaxWidth(200);
        fxd_contactUsButton.setMaxWidth(200);

        // Load background image
        FileInputStream fxd_fileInputStream = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/VarunImg/th10.jpg");
        Image fxd_image1 = new Image(fxd_fileInputStream);
        ImageView fxd_imageView1 = new ImageView(fxd_image1);
        fxd_imageView1.setFitHeight(1100);
        fxd_imageView1.setFitWidth(1900);
        VBox fxd_vb1 = new VBox(fxd_imageView1);
        fxd_vb1.setLayoutX(400);
        fxd_vb1.setLayoutY(300);

        // Create BorderPane for navbar
        BorderPane fxd_borderPane_nav = new BorderPane();
        fxd_borderPane_nav.setCenter(fxd_navbar1);

        // Create footer with label and center it at the bottom
        Label fxd_footerLabel = new Label("© 2024 Feed-N-Joy. All rights reserved.");
        fxd_footerLabel.setStyle("-fx-font-size: 14px; -fx-text-fill: #FFFFE0;");
        HBox fxd_footer = new HBox(fxd_footerLabel);
        fxd_footer.setStyle("-fx-background-color: rgba(0, 0, 0, 0.5); -fx-padding: 10px;");
        fxd_footer.setAlignment(Pos.CENTER_LEFT);

        // Create main layout with BorderPane
        BorderPane fxd_mainLayout = new BorderPane();
        fxd_mainLayout.setCenter(fxd_borderPane_nav);
        fxd_mainLayout.setRight(fxd_layout);
        fxd_mainLayout.setBottom(fxd_footer); // Add footer to the bottom
        BorderPane.setAlignment(fxd_footer, Pos.CENTER_LEFT); // Center the footer

        // Create StackPane with background image, navbar, and main layout
        StackPane fxd_stackPane = new StackPane(fxd_imageView1, fxd_borderPane_nav, fxd_mainLayout);

        // Create another BorderPane for stacking
        BorderPane fxd_borderpane3 = new BorderPane();
        fxd_borderpane3.setTop(fxd_borderPane_nav);
        fxd_borderpane3.setRight(fxd_mainLayout);
        fxd_borderpane3.setCenter(fxd_stackPane);

        // Create StackPane for final layout
        StackPane fxd_stackPane3 = new StackPane(fxd_imageView1, fxd_borderpane3);

        // Group the final layout and set it in a ScrollPane

        Group fxd_group = new Group(fxd_stackPane3);
        fxd_scrollpane = new ScrollPane(fxd_group);
        fxd_scrollpane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        fxd_scrollpane.setHbarPolicy(ScrollBarPolicy.NEVER);
    }
    

    public ScrollPane getView(){

             return fxd_scrollpane;
    }

}
