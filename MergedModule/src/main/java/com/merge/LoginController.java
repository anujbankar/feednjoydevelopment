package com.merge;

import java.io.FileInputStream;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class LoginController extends Application{

    private Stage pStage;

    private Scene  HomepageScene;
    private  HomePage homePage;

    private Scene AboutUsScene;
    private AboutUsPage aboutUs;

    private Scene ContactUsScene;
    private ContactUs contactUs;

    private Scene StoriesScene;
    private Stories stories;

   
    @Override

    public void start(Stage pStage)throws Exception{

        FileInputStream fileInputStream = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Gauri__Img/hlogo.png");

        Screen screen = Screen.getPrimary();
        double screenWidth = screen.getBounds().getWidth();
        double screenHeight = screen.getBounds().getHeight();


        this.pStage = pStage ;

        homePage = new HomePage(this);
        HomepageScene = new Scene(homePage.getView(),screenHeight,screenWidth);

        aboutUs = new AboutUsPage(this);
        AboutUsScene = new Scene(aboutUs.getView(),screenHeight,screenWidth);

        contactUs = new ContactUs(this);
        ContactUsScene = new Scene(contactUs.getView(),screenHeight,screenWidth);

        stories = new Stories(this);
        StoriesScene = new Scene(stories.getView(),screenHeight,screenWidth);

        pStage.setTitle("Feed-n-joy");
        pStage.setScene(HomepageScene);
        pStage.getIcons().add(new Image(fileInputStream));
        pStage.show();
    }
    
    public void NavigateToHomePage(){
                 
        pStage.setScene(HomepageScene);

    }

     public void NavigateToAboutUs(){

             pStage.setScene(AboutUsScene);

    }

    public void NavigateToContactUs(){

            pStage.setScene(ContactUsScene);
    }
    
    public void NavigateToStories(){

            pStage.setScene(StoriesScene);
            
    }

}
