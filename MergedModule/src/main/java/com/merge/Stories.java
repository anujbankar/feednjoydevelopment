package com.merge;

//Author : Monika Bhosale

import java.io.FileInputStream;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class Stories{

        private LoginController loginController;

        private ScrollPane fxd_scroll;

        public Stories(LoginController loginController)throws Exception{

                this.loginController = loginController; 

                initialize();

        }

public void initialize()throws Exception{    
        
    // set background image 
        FileInputStream fxd_back = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Monika_Img/ba.jpg");
        Image fxd_bc=new Image(fxd_back);
        ImageView fxd_iv=new ImageView(fxd_bc);
        fxd_iv.setFitWidth(2000);
        fxd_iv.setFitHeight(1400);

    // Buttons for navbar
        Button fxd_homeButton = createStyledButton("Home");

        fxd_homeButton.setOnAction(e->{

            loginController.NavigateToHomePage();

        });


        Button fxd_aboutButton = createStyledButton("About");

        fxd_aboutButton.setOnAction(e->{

            loginController.NavigateToAboutUs();;

        });

        Button fxd_contactButton = createStyledButton("Contact");

        fxd_contactButton.setOnAction(e->{

            loginController.NavigateToContactUs();;

        });

        Button fxd_registerButton = createStyledButton("Register");

        // fxd_registerButton.setOnAction(e->{

        //     fxd_content.setText(" Register as");

        // });

        Button fxd_storiesButton = createStyledButton("Stories");

        fxd_storiesButton.setOnAction(e->{

            loginController.NavigateToStories();

        });
    
    //Image of logo
        FileInputStream fxd_lo = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Gauri__Img/hlogo.png");
        Image fxd_logo=new Image(fxd_lo);
        ImageView fxd_logov= new ImageView(fxd_logo);
        fxd_logov.setFitHeight(170);
        fxd_logov.setFitWidth(400);

    // Hbox for navbar    
        HBox fxd_navbar = new HBox(fxd_homeButton, fxd_aboutButton, fxd_registerButton, fxd_contactButton,fxd_storiesButton);
        fxd_navbar.setSpacing(50);
        fxd_navbar.setPrefHeight(20);
        fxd_navbar.setAlignment(Pos.CENTER_RIGHT);
    //hbox for logo+navbar
        HBox fxd_hbox1_nav=new HBox(600,fxd_logov,fxd_navbar);

    //Label of page
        Label fxd_Heading=new Label("Our Stories");
        fxd_Heading.setFont(Font.font("Times New Roman", FontWeight.BOLD, 70)); 
        fxd_Heading.setTextFill(Color.WHITE);
    //Hbox for Label 
        VBox fxd_vb=new VBox(fxd_Heading);
        fxd_vb.setAlignment(Pos.TOP_CENTER); 
        
    //Label of 1st Donor
        Label fxd_lb1=new Label("Peshvai Restaurant");
        fxd_lb1.setFont(Font.font("Arial", FontWeight.BOLD, 30)); 
        fxd_lb1.setTextFill(Color.WHITE);
        fxd_lb1.setAlignment(Pos.TOP_LEFT);
    //Image of Hotel 
        FileInputStream fxd_Pe = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Monika_Img/img1.jpg");
        Image fxd_ig1=new Image(fxd_Pe);
        ImageView fxd_im1=new ImageView(fxd_ig1);
        fxd_im1.setFitWidth(250);
        fxd_im1.setFitHeight(200);
    //Content of Hotel(Peshvai)
        Label fxd_ll1=new Label("Peshvai Restaurant, a renowned establishment in the comm-\nunity has once again demonstrated its commitment \n to social responsibility by donating 40 kg \nof food to NGO. This generous contribution \nis part of hotel's ongoing efforts to support the community.");
        fxd_ll1.setFont(Font.font("Arial", FontWeight.BOLD, 18)); 
        fxd_ll1.setTextFill(Color.WHITE);
    //Vbox of Donor1
        VBox fxd_vb1=new VBox(10,fxd_lb1,fxd_im1,fxd_ll1);
        fxd_vb1.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 20");
        fxd_vb1.setAlignment(Pos.CENTER);

    //Label of 2nd Donor
        Label fxd_lb2=new Label("Manoj Patil");
        fxd_lb2.setFont(Font.font("Arial", FontWeight.BOLD, 30)); 
        fxd_lb2.setTextFill(Color.WHITE);
        fxd_lb2.setAlignment(Pos.TOP_LEFT);
    //Image of 2nd dodnor
        FileInputStream fxd_Ma = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Monika_Img/img4.jpg");
        Image fxd_ig2=new Image(fxd_Ma);
        ImageView fxd_im2=new ImageView(fxd_ig2);
        fxd_im2.setFitWidth(220);
        fxd_im2.setFitHeight(200);
    //Content of 2nd donor
        Label fxd_ll2=new Label("Manoj Patil, a respected organizer known for his\n large-scale events recently hosted a significant event\n in Nagpur. As the event concluded, it became apparent\n that a substantial amount of food remained unused.\n Recognizing the opportunity to make a positive impact\n,He decided to donate the food to a NGO.");
        fxd_ll2.setFont(Font.font("Arial", FontWeight.BOLD, 18)); 
        fxd_ll2.setTextFill(Color.WHITE);
    //Vbox of Donor2
        VBox fxd_vb2=new VBox(10,fxd_lb2,fxd_im2,fxd_ll2);
        fxd_vb2.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 20");
        fxd_vb2.setAlignment(Pos.CENTER);

    //Label of 3rd Donor
        Label fxd_lb3=new Label("Samarth Hotel");
        fxd_lb3.setFont(Font.font("Arial", FontWeight.BOLD, 30)); 
        fxd_lb3.setTextFill(Color.WHITE);
        fxd_lb3.setAlignment(Pos.TOP_LEFT);
    //Image of Donor3
        FileInputStream fxd_Sa = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Monika_Img/s.jpg");
        Image fxd_ig3=new Image(fxd_Sa);
        ImageView fxd_im3=new ImageView(fxd_ig3);
        fxd_im3.setFitWidth(250);
        fxd_im3.setFitHeight(200);
    //Content of Donor3
        Label fxd_ll3=new Label("At Hotel Samarth, we are committed to making a positive \nimpact in our community. As part of our social \nresponsibility program, we donate leftover food to local NGOs.\nThis initiative helps reduce food wastage &supports those \nin need, ensuring that good food reaches the less fortunate.");
        fxd_ll3.setFont(Font.font("Arial", FontWeight.BOLD, 18)); 
        fxd_ll3.setTextFill(Color.WHITE);
    //Vbox of Donor3
        VBox fxd_vb3=new VBox(10,fxd_lb3,fxd_im3,fxd_ll3);
        fxd_vb3.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 20");
        fxd_vb3.setAlignment(Pos.CENTER);
      
    //Hbox of all 3 Vbox(dono1,donor2,donor3)
        HBox fxd_hbox1= new HBox(80,fxd_vb1,fxd_vb2,fxd_vb3);  
        fxd_hbox1.setAlignment(Pos.CENTER);
      
    //Label of 4th Donor
        Label fxd_lb4=new Label("Prasad Thakur");
        fxd_lb4.setFont(Font.font("Arial", FontWeight.BOLD, 30)); 
        fxd_lb4.setTextFill(Color.WHITE);
        fxd_lb4.setAlignment(Pos.TOP_LEFT);
    //Image of 4th Donor 
        FileInputStream fxd_Pr = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Monika_Img/img4.jpg");
        Image fxd_ig4=new Image(fxd_Pr);
        ImageView fxd_im4=new ImageView(fxd_ig4);
        fxd_im4.setPreserveRatio(true);
        fxd_im4.setFitWidth(240);
        fxd_im4.setFitHeight(200);
    //Content of 4th Donor
        Label fxd_ll4=new Label("Prasad Thakur, a well-known event organizer, recently \norchestrated a grand wedding event. After the festivities\nconcluded, there was a 50 kg of food remaining.\n Instead of letting it go to waste, Prasad Thakur\ndecided to donate the surplus food to a local NGO.");
        fxd_ll4.setFont(Font.font("Arial", FontWeight.BOLD, 18)); 
        fxd_ll4.setTextFill(Color.WHITE);
    //Vbox of Donor 4
        VBox fxd_vb4=new VBox(10,fxd_lb4,fxd_im4,fxd_ll4);  
        fxd_vb4.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 20");
        fxd_vb4.setAlignment(Pos.CENTER);
      
    //Label of 5th Donor
        Label fxd_lb5=new Label("Restaurant Taj Taste");
        fxd_lb5.setFont(Font.font("Arial", FontWeight.BOLD, 30)); 
        fxd_lb5.setTextFill(Color.WHITE);
        fxd_lb5.setAlignment(Pos.TOP_LEFT);
    //Image of 5th Donor
        FileInputStream fxd_ta = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Monika_Img/img5.jpeg");
        Image fxd_ig5=new Image(fxd_ta);
        ImageView fxd_im5=new ImageView(fxd_ig5);
        fxd_im5.setFitWidth(240);
        fxd_im5.setFitHeight(200);
    //Content of Donor5
        Label fxd_ll5=new Label("At Taj Taste, we believe in the power of community and the \nimportance of giving back. As part of our commitment to social \nresponsibility, we are proud to introduce our Community Food \nDonation Initiative. This program is designed to ensure that the \ndelicious  meals we reach those in need.");
        fxd_ll5.setFont(Font.font("Arial", FontWeight.BOLD, 18)); 
        fxd_ll5.setTextFill(Color.WHITE);
    //Vbox of Donor 5
        VBox fxd_vb5=new VBox(10,fxd_lb5,fxd_im5,fxd_ll5); 
        fxd_vb5.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 20");  
        fxd_vb5.setAlignment(Pos.CENTER);
   
    //Label of 6th Donor
        Label fxd_lb6=new Label("Hotel Royal");
        fxd_lb6.setFont(Font.font("Arial", FontWeight.BOLD, 30)); 
        fxd_lb6.setTextFill(Color.WHITE);
        fxd_lb6.setAlignment(Pos.TOP_LEFT);
    //Image of 6th donor 
        FileInputStream fxd_Ro = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/feednjoydevelopment/MergedModule/src/main/resources/Monika_Img/img6.jpg");
        Image fxd_ig6=new Image(fxd_Ro);
        ImageView fxd_im6=new ImageView(fxd_ig6);
        fxd_im6.setFitWidth(250);
        fxd_im6.setFitHeight(200);
    //Content of donor 6
        Label fxd_ll6=new Label("At Hotel Royal,Recently,hosted a joyous birthday celebration\n As a committee  we are deeply grateful to Hotel Royal for their \ngenerous donation of 40 kg of biryani following a recent celebration.\nSuch contributions play a crucial role in our mission to support\n the underserved members of our community.");
        fxd_ll6.setFont(Font.font("Arial", FontWeight.BOLD, 18)); 
        fxd_ll6.setTextFill(Color.WHITE);
    //Vbox of Donor6
        VBox fxd_vb6=new VBox(10,fxd_lb6,fxd_im6,fxd_ll6);
        fxd_vb6.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 20");
        fxd_vb6.setAlignment(Pos.CENTER);

    //Hbox of all 3 Vbox(Donor4 ,donor5,donor6)
        HBox fxd_hbox2= new HBox(70,fxd_vb4,fxd_vb5,fxd_vb6);  
        fxd_hbox2.setAlignment(Pos.CENTER);
      
    //VBox for both HBox 
        VBox fxd_final = new VBox(40,fxd_hbox1,fxd_hbox2);
    
    //BorderPane for set Navbar at top    
        BorderPane fxd_Bp=new BorderPane();
        fxd_Bp.setTop(fxd_hbox1_nav);
    // all content of page
        VBox fxd_all=new VBox(50,fxd_Bp,fxd_vb,fxd_final);

    //footer content
        Label footerLabel = new Label("© 2024 Feed-N-Joy. All rights reserved.");
        footerLabel.setStyle("-fx-font-size: 20px; -fx-text-fill:BLACK;");
        HBox footer = new HBox(footerLabel);
        footer.setStyle("-fx-background-color:GREY; -fx-padding: 20px;");
        footer.setAlignment(Pos.CENTER);
    //borderpane for footer
        BorderPane brFooter =new BorderPane();
        brFooter.setBottom(footer);

    //give background image and all content to StackPane
        StackPane  fxd_gr= new  StackPane(fxd_iv,fxd_all,brFooter);
        fxd_scroll= new ScrollPane(fxd_gr);
        fxd_scroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
    }
    // button style 
        public Button createStyledButton(String text) {
            Button button = new Button(text);
            button.setPrefHeight(50);
            button.setPrefWidth(120);
            button.setStyle("-fx-background-color:GREY; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 35;");
            button.setOnMouseEntered(e -> button.setStyle("-fx-background-color:BLACK; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 20;"));
            button.setOnMouseExited(e -> button.setStyle("-fx-background-color:GREY; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 20;"));
            return button;
    }

    public ScrollPane getView(){

            return fxd_scroll;
    }
}
    


