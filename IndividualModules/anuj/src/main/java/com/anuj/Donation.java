package com.anuj;

import java.io.FileInputStream;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/*AuthorName: Anuj.Bankar
 * Description": Volunteer Donation Form
 * Input: Name of Volunteer , Name of Food , Enter Quantity to Donate , PickUp Address , Cooking Date , FoodType , Submit Button
 * Output: Donation Confirmed
 */



public class Donation extends Application {
   

    public static void main(String[] args) throws Exception {

          launch(args);
    }

    public void start(Stage stage)throws Exception{

        /*Stage Setting */

        stage.setTitle("LoginIn");
        stage.setHeight(1000);
        stage.setWidth(1000);
        stage.setResizable(true);

        FileInputStream fileInputStream1 = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/anuj/src/main/resources/Anuj/donationImg.jpg");
        Image Image0 = new Image(fileInputStream1);
        ImageView ImageView = new ImageView(Image0);
        // ImageView.setFitHeight(230);
        ImageView.setPreserveRatio(true);
        ImageView.setFitWidth(320);


        /*Background Image Setting */
        FileInputStream fileInputStream2 = new FileInputStream("C:/Users/ANUJKISHORBANKAR/Desktop/FXDesigners/anuj/src/main/resources/Anuj/VolunteerLoginBackground.jpg");
        Image Image1 = new Image(fileInputStream2) ;  
        BackgroundImage backgroundImage = new BackgroundImage(Image1,
        BackgroundRepeat.NO_REPEAT,
        BackgroundRepeat.NO_REPEAT,
        BackgroundPosition.DEFAULT,
        new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, false, true));

        Background background = new Background(backgroundImage);

          
        /*Textfield for Name*/

        TextField TextField1 = new TextField();
        TextField1.setPromptText("Enter Name of Volunteer");
        TextField1.setFocusTraversable(false);
        TextField1.setFont(Font.font("Roboto",FontWeight.BOLD,15));
        TextField1.setPrefWidth(330);
        TextField1.setStyle("-fx-background-radius:5");
        HBox HBox1 = new HBox(TextField1);
        HBox1.setAlignment(Pos.CENTER);


         /*Textfield for Name of Food*/

        TextField TextField2 = new TextField();
        TextField2.setPromptText("Enter Name of Food");
        TextField2.setFocusTraversable(false);
        TextField2.setFont(Font.font("Roboto",FontWeight.BOLD,15));
        TextField2.setPrefWidth(330);
        TextField2.setStyle("-fx-background-radius:5");
        HBox HBox2 = new HBox(TextField2);
        HBox2.setAlignment(Pos.CENTER);
        
         /*Textfield for Quantity to Donate*/

        TextField TextField3 = new TextField();
        TextField3.setPromptText("Enter Quantity to Donate(in kgs)");
        TextField3.setFocusTraversable(false);
        TextField3.setFont(Font.font("Roboto",FontWeight.BOLD,15));
        TextField3.setPrefWidth(330);
        TextField3.setStyle("-fx-background-radius:5");
        HBox HBox3 = new HBox(TextField3);
        HBox3.setAlignment(Pos.CENTER);

        /*TextArea for Address*/

        TextArea TextArea = new TextArea();
        TextArea.setPromptText("Enter Pick-Up Address(*Optional)");
        TextArea.setFocusTraversable(false);
        TextArea.setFont(Font.font("Roboto",FontWeight.BOLD,15));
        TextArea.setPrefSize(330, 100);
        TextArea.setWrapText(true);
        HBox HBox4 = new HBox(TextArea);
        HBox4.setAlignment(Pos.CENTER);

         /*Cooking Date Info*/
        
        DatePicker DatePicker = new DatePicker();
        DatePicker.setPromptText("Cooking Date");
        DatePicker.setFocusTraversable(false);
        VBox VBox3 = new VBox(DatePicker);
        VBox3.setAlignment(Pos.CENTER);
       
         /*Label to Select Food Type*/

        Label Label = new Label("Select Food Type");
        Label.setFont(Font.font("Time New Roman",FontWeight.BOLD,17));

        VBox VBox5 = new VBox(Label);
        VBox5.setAlignment(Pos.CENTER);

        /*Choice Box for Veg/Non-Veg Option*/

        ChoiceBox<String> choiceBox = new ChoiceBox();
            
            ObservableList<String> list  = choiceBox.getItems();

            list.addAll("Vegeterian","Non-Vegeterian");
            
        /*VBox for choiceBox */
        VBox VBox1 = new VBox(choiceBox);

        /*CheckBox */

        CheckBox checkBox2 = new CheckBox("Use my current location");
        checkBox2.setFont(Font.font(15));

        VBox VBox4 = new VBox(checkBox2);
        VBox4.setAlignment(Pos.CENTER);
        VBox4.setPadding(new Insets(10));

        VBox1.setAlignment(Pos.CENTER);

        /*Submit Button Code*/

        Button button = new Button("Submit");
        button.setFont(Font.font("Roboto",FontWeight.BOLD,15));
        button.setPadding(new Insets(9));
        button.setStyle("-fx-background-color:LIGHTGREY;-fx-background-radius:10");
        
        VBox VBox2 = new VBox(button);
        VBox2.setAlignment(Pos.CENTER);

        /*Complete VBox for storing images,texts etc.. */
       
        VBox VBox = new VBox(16,ImageView,HBox1,HBox2,HBox3,HBox4,VBox3,VBox5,VBox1,VBox4,VBox2);
        VBox.setAlignment(Pos.CENTER);
        VBox.setPrefSize(515, 775);
        VBox.setStyle("-fx-background-radius:18;-fx-background-color:LIGHTBLUE");
        VBox.setLayoutX(230);
        VBox.setLayoutY(190);
        VBox.setPadding(new Insets(50));

        /*Group storing the VBox*/

        Group grp = new Group();
        grp.getChildren().addAll(VBox);

         /*StackPne for setting background & grp*/
       
        StackPane root = new StackPane();
        root.setBackground(background);
        root.getChildren().add(grp);

        Scene sc = new Scene(root,1000,1000);
                
        stage.setScene(sc);


        stage.show();
    }
}
